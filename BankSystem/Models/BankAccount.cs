﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BankSystem.Models
{
    public class BankAccount
    {
        public BankAccount()
        {
            BankCards = new List<BankCard>();
            SentTransactions = new List<Transaction>();
            RecievedTransactions = new List<Transaction>();
        }
        [Key]
        public int ID { get; set; }
        public string Nr { get; set; }
        public long OpenTimestamp { get; set; }
        public double Balance { get; set; }
        public double MonthlyTariff { get; set; }
        public BankAccountStatus BankAccountStatus { get; set; }
        public bool InDebt { get; set; }

        public int ClientID { get; set; }
        [Required]
        public virtual Client Client { get; set; }
        public virtual ICollection<BankCard> BankCards { get; set; }
        public virtual ICollection<Transaction> SentTransactions { get; set; }
        public virtual ICollection<Transaction> RecievedTransactions { get; set; }
    }
    public enum BankAccountStatus
    {
        blocked,
        closed,
        active
    }
}
