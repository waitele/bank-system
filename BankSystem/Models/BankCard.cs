﻿using System.ComponentModel.DataAnnotations;

namespace BankSystem.Models
{
    public class BankCard
    {
        [Key]
        public int ID { get; set; }
        public string Nr { get; set; }
        public long ExpirationTimestamp { get; set; }
        public string Cvc { get; set; }
        public bool Contactless { get; set; }
        public bool IsActive { get; set; }

        public int BankCardTypeID { get; set; }
        [Required]
        public virtual BankCardType BankCardType { get; set; }
        public int BankAccountID { get; set; }
        [Required]
        public virtual BankAccount BankAccount { get; set; }
    }
}
