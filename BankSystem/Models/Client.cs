﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BankSystem.Models
{
    public class Client
    {
        public Client()
        {
            BankAccounts = new List<BankAccount>();
        }
        public int ID { get; set; }
        public string PersonCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Adress { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public long BirthDateTimestamp { get; set; }
        
        public virtual ICollection<BankAccount> BankAccounts { get; set; }
        public string UserID { get; set; }
        [Required]
        public virtual User User { get; set; }
    }
}
