﻿using System.ComponentModel.DataAnnotations;

namespace BankSystem.Models
{
    public class Transaction
    {
        public int ID { get; set; }
        public double Amount { get; set; }
        public long Date { get; set; }
        public string Purpose { get; set; }
        public string PaymentCode { get; set; }

        public int SenderID { get; set; }
        [Required]
        public virtual BankAccount Sender { get; set; }
        public int RecieverID { get; set; }
        [Required]
        public virtual BankAccount Reciever { get; set; }
    }
}
