﻿using System.ComponentModel.DataAnnotations;

namespace BankSystem.Models
{
    public class PensionAccount : BankAccount
    {
        public double TransferAmount { get; set; }
        public long TransferDay { get; set; }

        public int TierID { get; set; }
        [Required]
        public virtual Tier Tier { get; set; }
    }
}
