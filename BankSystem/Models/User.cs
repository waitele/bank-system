﻿using Microsoft.AspNetCore.Identity;

namespace BankSystem.Models
{
    public class User : IdentityUser
    {
        public long CreatedDateTimestamp { get; set; }
        public bool IsAdmin { get; set; }

        public int? ClientId { get; set; }
        public virtual Client Client { get; set; }
    }
}
