﻿namespace BankSystem.Models
{
    public class SavingsAccount : BankAccount
    {
        public double TransferAmount { get; set; }
        public long TransferDay { get; set; }
        public double Interest { get; set; }
    }
}
