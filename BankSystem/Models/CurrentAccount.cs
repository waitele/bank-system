﻿namespace BankSystem.Models
{
    public class CurrentAccount : BankAccount
    {
        public double TransactionTariff { get; set; }
        public double Overdraft { get; set; }
        public double Interest { get; set; }
    }
}
