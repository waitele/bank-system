﻿using System.Collections.Generic;

namespace BankSystem.Models
{
    public class BankCardType
    {
        public BankCardType()
        {
            BankCards = new List<BankCard>();
        }
        public int ID { get; set; }
        public CardType CardType { get; set; }
        public double Tariff { get; set; }
        public double Limit { get; set; }
        public Organization Organization { get; set; }
        public bool IsInsured { get; set; }

        public virtual ICollection<BankCard> BankCards { get; set; }
    }
    public enum CardType
    {
        silver,
        standard,
        golden,
        platinum,
        black
    }
    public enum Organization
    {
        visa,
        mastercard
    }
}
