﻿using System.Collections.Generic;

namespace BankSystem.Models
{
    public class Tier
    {
        public Tier()
        {
            PensionAccounts = new List<PensionAccount>();
        }
        public int ID { get; set; }
        public TierType TierType { get; set; }
        public double Percent { get; set; }

        public virtual ICollection<PensionAccount> PensionAccounts { get; set; }
    }
    public enum TierType
    {
        first_pillar,
        second_pillar,
        third_pillar
    }
}
