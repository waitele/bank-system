﻿using BankSystem.Models;
using BankSystem.Repositories;
using BankSystem.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BankSystem.Controllers
{
    public class BankAccountController : Controller
    {
        private readonly Repository _repository;
        private readonly UserManager<User> _userManager;

        public BankAccountController(Repository repository, UserManager<User> userManager)
        {
            _repository = repository;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            if (User.IsInRole("Admin"))
                return RedirectToAction("GetAllAccounts");
            else
                return RedirectToAction("GetMyAccounts");
        }

        [Authorize(Roles = "Client")]
        public IActionResult GetMyAccounts()
        {
            Console.WriteLine();
            List<CurrentAccount> currentBankAccounts = _repository.CurrentAccounts.Where(x => x.ClientID == _userManager.GetUserAsync(User).Result.Client.ID).ToList();
            List<SavingsAccount> savingsBankAccounts = _repository.SavingsAccounts.Where(x => x.ClientID == _userManager.GetUserAsync(User).Result.Client.ID).ToList();
            List<PensionAccount> pensionBankAccounts = _repository.PensionAccounts.Where(x => x.ClientID == _userManager.GetUserAsync(User).Result.Client.ID).ToList();
            BankAccountList allBankAccounts = new BankAccountList()
            {
                allCurrentAccounts = currentBankAccounts,
                allPensionAccounts = pensionBankAccounts,
                allSavingsAccounts = savingsBankAccounts

            };
            
            return View("/Views/BankAccount/UserAccountView.cshtml", allBankAccounts);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult GetAllAccounts()
        {
            IEnumerable<BankAccount> bankaccs = _repository.BankAccounts;
            return View("/Views/BankAccount/AdminBankAccountsView.cshtml", bankaccs);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Activate(int? id)
        {
            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var obj = _repository.BankAccounts.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            obj.BankAccountStatus = BankAccountStatus.active;
            _repository.SaveChanges();
            return RedirectToAction("/Views/BankAccount/AdminBankAccountsView.cshtml");
        }

        [Authorize(Roles = "Client")]
        public IActionResult AddAccountIndex()
        {
            return View();
        }

        public IActionResult RegisterAccount(string? accountType, string? pillarType)
        {
            string GenerateBankNumber()
            {
                string bankNumber = null;
                var bankAccounts = _repository.BankAccounts;
                Random random = new Random();
                bankNumber += "LT";
                bankNumber += "01";
                bankNumber += "99999";
                bankNumber += random.Next(100000, 999999).ToString();
                bankNumber += random.Next(10000, 99999).ToString();

                foreach (var bankAccount in bankAccounts)
                {
                    if (bankNumber == bankAccount.Nr)
                    {
                        bankNumber = "";
                        bankNumber += "LT";
                        bankNumber += "01";
                        bankNumber += "99999";
                        bankNumber += random.Next(100000, 999999).ToString();
                        bankNumber += random.Next(10000, 99999).ToString();
                    }
                }
                return bankNumber;
            }

            if (accountType == "currentAccount")
            {
                CurrentAccount currentAccount = new CurrentAccount()
                {
                    TransactionTariff = 0.30,
                    Overdraft = 500,
                    Interest = 0.00,
                    Nr = GenerateBankNumber(),            
                    OpenTimestamp = DateTime.Today.Ticks,
                    Balance = 0,
                    MonthlyTariff = 2.70,
                    BankAccountStatus = BankAccountStatus.blocked,
                    InDebt = false,
                    Client = _userManager.GetUserAsync(User).Result.Client
                };
                _repository.CurrentAccounts.Add(currentAccount);
                _repository.SaveChanges();
            }
            else if (accountType == "savingsAccount")
            {
                SavingsAccount savingsAccount = new SavingsAccount()
                {
                    TransferAmount = 50,
                    TransferDay = 5,
                    Interest = 0.04,
                    Nr = GenerateBankNumber(),
                    OpenTimestamp = DateTime.Today.Ticks,
                    Balance = 0,
                    MonthlyTariff = 0.50,
                    BankAccountStatus = BankAccountStatus.blocked,
                    InDebt = false,
                    Client = _userManager.GetUserAsync(User).Result.Client
                };
                _repository.SavingsAccounts.Add(savingsAccount);
                _repository.SaveChanges();
            }

            else if (accountType == "pensionAccount")
            {
                if (pillarType == "first_pillar")
                {
                    PensionAccount pensionAccount = new PensionAccount()
                    {
                        TransferAmount = 20,
                        TransferDay = 5,
                        Tier = new Tier()
                        {
                            TierType = TierType.first_pillar,
                            Percent = 1.5

                        },
                        Nr = GenerateBankNumber(),     
                        OpenTimestamp = DateTime.Today.Ticks,
                        Balance = 0,
                        MonthlyTariff = 0,
                        BankAccountStatus = BankAccountStatus.blocked,
                        InDebt = false,
                        Client = _userManager.GetUserAsync(User).Result.Client
                    };
                    _repository.PensionAccounts.Add(pensionAccount);
                    _repository.SaveChanges();
                }
                else if (pillarType == "second_pillar")
                {
                    PensionAccount pensionAccount = new PensionAccount()
                    {
                        TransferAmount = 30,
                        TransferDay = 5,
                        Tier = new Tier()
                        {

                            TierType = TierType.second_pillar,
                            Percent = 3
                        },
                        Nr = GenerateBankNumber(),
                        OpenTimestamp = DateTime.Today.Ticks,
                        Balance = 0,
                        MonthlyTariff = 0,
                        BankAccountStatus = BankAccountStatus.blocked,
                        InDebt = false,
                        Client = _userManager.GetUserAsync(User).Result.Client
                    };
                    _repository.PensionAccounts.Add(pensionAccount);
                    _repository.SaveChanges();
                }

                else if (pillarType == "third_pillar")
                {
                    PensionAccount pensionAccount = new PensionAccount()
                    {
                        TransferAmount = 50,
                        TransferDay = 5,
                        Tier = new Tier()
                        {
                            TierType = TierType.third_pillar,
                            Percent = 3.5

                        },
                        Nr = GenerateBankNumber(),
                        OpenTimestamp = DateTime.Today.Ticks,
                        Balance = 0,
                        MonthlyTariff = 0,
                        BankAccountStatus = BankAccountStatus.blocked,
                        InDebt = false,
                        Client = _userManager.GetUserAsync(User).Result.Client
                    };
                    _repository.PensionAccounts.Add(pensionAccount);
                    _repository.SaveChanges();
                }


            }
            return RedirectToAction("GetMyAccounts");
        }



    }
}
