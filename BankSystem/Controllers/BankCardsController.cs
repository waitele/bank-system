﻿using BankSystem.Models;
using BankSystem.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BankSystem.Controllers
{
    [Authorize]
    public class BankCardsController : Controller
    {
        private readonly Repository _repository;
        private readonly UserManager<User> _userManager;

        public BankCardsController(Repository repository, UserManager<User> userManager)
        {
            _repository = repository;
            _userManager = userManager;
        }

        [Authorize(Roles = "Client")]
        public IActionResult Index()
        {
            IEnumerable<BankCard> bank = GetCurrentUserBankCards();
            return View(bank);
        }

        [Authorize(Roles = "Client")]
        public IActionResult Deactivate(int? id)
        {
            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var obj = _repository.BankCards.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            obj.IsActive = false;
            _repository.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Client")]
        public IActionResult AddCard()
        {
            IEnumerable<BankAccount> bankacc = GetCurrentUserBankAccounts();
            return View(bankacc);
        }

        private IEnumerable<BankCard> GetCurrentUserBankCards()
        {
            return _repository.BankCards.Where(x => x.BankAccount.ClientID == _userManager.GetUserAsync(User).Result.Client.ID);
        }

        private IEnumerable<BankAccount> GetCurrentUserBankAccounts()
        {
            return _repository.BankAccounts.Where(x => x.ClientID == _userManager.GetUserAsync(User).Result.Client.ID);
        }

        [Authorize(Roles = "Client")]
        public IActionResult SendAdditionRequest(string? cardType, string? contactless, string? bankAcc)
        {
            bool contact = false;
            if (cardType == "Taip")
                contact = true;

            var baseDate = new DateTime(1970, 01, 01);
            var toDate = DateTime.Today;
            var expirationDate = Convert.ToInt64(toDate.AddYears(3).Subtract(baseDate).TotalSeconds);

            BankAccount bankAccount = _repository.BankAccounts.Where(x => x.Nr == bankAcc).FirstOrDefault();

            if (Enum.TryParse(cardType, out CardType cardTypeEnum))
            {
                BankCard bankCard = new BankCard
                {
                    IsActive = true,
                    Nr = Guid.NewGuid().ToString(),
                    Contactless = contact,
                    BankAccount = bankAccount,
                    BankCardType = new BankCardType { CardType = cardTypeEnum },
                    Cvc = "597",
                    ExpirationTimestamp = expirationDate
                };
                _repository.BankCards.Add(bankCard);
                _repository.SaveChanges();
            }
            else
            {
                return NotFound(cardType);
            }

            return RedirectToAction("Index");
        }
    }
}
