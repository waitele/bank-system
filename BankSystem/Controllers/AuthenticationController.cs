﻿ using BankSystem.Areas.Identity.Pages.Account;
using BankSystem.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BankSystem.Controllers
{
    public class AuthenticationController : Controller
    {
        private SignInManager<User> _signInManager;
        private UserManager<User> _userManager;

        public AuthenticationController(SignInManager<User> signInManager, UserManager<User> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return CheckAuth(RedirectToAction("Login"));
        }

        public IActionResult Login()
        {
            return CheckAuth(View("Areas/Identity/Pages/Account/Login.cshtml"));
        }

        private IActionResult CheckAuth(IActionResult actionResult)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "BankAccount");
            }
            return actionResult;
        }

        public async Task<IActionResult> LoginAction(LoginModel model)
        {
            var returnAction = CheckAuth(null);
            if (returnAction != null)
            {
                return returnAction;
            }
            var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                return RedirectToAction("Login");
            }
        }

        [Authorize]
        public async Task<IActionResult> LogoutAction()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login");
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Register()
        {
            return View("Areas/Identity/Pages/Account/Register.cshtml");
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> RegisterAction(RegisterModel model)
        {
            var dateTime = Convert.ToDateTime(model.Birthday);
            long unixTime = ((DateTimeOffset)dateTime).ToUnixTimeSeconds();

            var user = new User
            {
                UserName = model.Username
            };
            var client = new Client
            {
                FirstName = model.Username,
                LastName = model.Usersurname,
                Adress = model.Address,
                Telephone = model.PhoneNumber,
                Email = model.Email,
                PersonCode = model.IndentificationNumber,
                BirthDateTimestamp = unixTime,
                User = user
            };
            user.Client = client;
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "Client");
                return RedirectToAction("Index", "BankCards");
            }
            return RedirectToAction("Register");
        }
    }
}
