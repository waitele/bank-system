﻿using BankSystem.Models;
using BankSystem.Repo;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BankSystem.Repositories
{
    public class Repository : IdentityDbContext<User, IdentityRole, string>
    {
        public Repository(DbContextOptions<Repository> options) : base(options)
        {

        }

        public DbSet<BankAccount> BankAccounts { get; set; }
        public DbSet<BankCard> BankCards { get; set; }
        public DbSet<BankCardType> BankCardTypes { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<CurrentAccount> CurrentAccounts { get; set; }
        public DbSet<PensionAccount> PensionAccounts { get; set; }
        public DbSet<SavingsAccount> SavingsAccounts { get; set; }
        public DbSet<Tier> Tiers { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite(@"Data Source=C:\BankSystemDB\banksystem.db");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BankAccount>(entity =>
            {
                entity.HasOne(e => e.Client).WithMany(e => e.BankAccounts);

                entity.HasMany(e => e.BankCards).WithOne(e => e.BankAccount);

                entity.HasMany(e => e.SentTransactions).WithOne(e => e.Sender);

                entity.HasMany(e => e.RecievedTransactions).WithOne(e => e.Reciever);
            });

            modelBuilder.Entity<BankCard>(entity =>
            {
                entity.HasOne(e => e.BankCardType).WithMany(e => e.BankCards);

                entity.HasOne(e => e.BankAccount).WithMany(e => e.BankCards);
            });

            modelBuilder.Entity<BankCardType>(entity =>
            { 
                entity.HasMany(e => e.BankCards).WithOne(e => e.BankCardType);
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.HasOne(e => e.User).WithOne(e => e.Client).HasForeignKey<Client>(e => e.UserID);

                entity.HasMany(e => e.BankAccounts).WithOne(e => e.Client);
            });

            modelBuilder.Entity<PensionAccount>(entity =>
            {
                entity.HasOne(e => e.Tier).WithMany(e => e.PensionAccounts);
            });

            modelBuilder.Entity<Tier>(entity =>
            {
                entity.HasMany(e => e.PensionAccounts).WithOne(e => e.Tier);
            });

            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.HasOne(e => e.Sender).WithMany(e => e.SentTransactions);

                entity.HasOne(e => e.Reciever).WithMany(e => e.RecievedTransactions);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasOne(e => e.Client).WithOne(e => e.User);
            });

            modelBuilder.Seed();
        }
    }
}
