﻿using BankSystem.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace BankSystem.Repo
{
    public static class ModelBuilderSeedExtension
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            BankCardType bankCardTypeGold = new BankCardType
            {
                ID = -1,
                CardType = CardType.golden,
                Tariff = 1.99,
                Limit = 300,
                Organization = Organization.visa,
                IsInsured = false
            };

            User user = new User
            {
                Id = "-1",
                UserName = "kazkas",
            };

            Client client = new Client
            {
                ID = -1,
                PersonCode = "354546545364",
                FirstName = "Žiurkius",
                LastName = "Jacius",
                Adress = "Bangų 6",
                Telephone = "+37067864563",
                Email = "ziurkenas@gmail.com",
                BirthDateTimestamp = 1610857710,
                UserID = "-1"
            };

            BankAccount bankAccount1 = new BankAccount
            {
                ID = -1,
                Balance = 12.14,
                Nr = "987654321",
                BankAccountStatus = BankAccountStatus.active,
                ClientID = -1,
                InDebt = false,
                MonthlyTariff = 0.7,
                OpenTimestamp = ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds(),
            };

            BankAccount bankAccount2 = new BankAccount
            {
                ID = -2,
                Balance = 500,
                Nr = "191817161514",
                BankAccountStatus = BankAccountStatus.active,
                ClientID = -1,
                InDebt = false,
                MonthlyTariff = 0.7,
                OpenTimestamp = ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds(),
            };


            BankCard bankCard = new BankCard
            {
                ID = -1,
                ExpirationTimestamp = ((DateTimeOffset)DateTime.Now.AddDays(365)).ToUnixTimeSeconds(),
                Contactless = true,
                Cvc = "743",
                IsActive = true,
                Nr = "123456789", 
                BankAccountID = -1, 
                BankCardTypeID = -1 
            };

            Transaction transaction = new Transaction
            {
                ID = -1,
                Amount = 50,
                Date = 1620047710,
                PaymentCode = "37650",
                Purpose = "Uz nuostabu univero mokyma",
                RecieverID = -1,
                SenderID = -2
            };

            modelBuilder.Entity<BankCardType>().HasData(bankCardTypeGold);
            modelBuilder.Entity<User>().HasData(user);
            modelBuilder.Entity<Client>().HasData(client);
            modelBuilder.Entity<BankAccount>().HasData(bankAccount1);
            modelBuilder.Entity<BankAccount>().HasData(bankAccount2);
            modelBuilder.Entity<BankCard>().HasData(bankCard);
            modelBuilder.Entity<Transaction>().HasData(transaction);
        }
    }
}
