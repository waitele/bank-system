﻿using BankSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.ViewModel
{
    public class BankAccountList
    {

        public List <CurrentAccount> allCurrentAccounts { get; set; }
        public List <SavingsAccount> allSavingsAccounts { get; set; }
        public List <PensionAccount> allPensionAccounts { get; set; }
    }
}
