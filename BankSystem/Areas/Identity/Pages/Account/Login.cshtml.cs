﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;

namespace BankSystem.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LoginModel
    {
        [Required]
        [Display(Name = "Prisijungimo vardas")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Slaptažodis")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Prisiminti?")]
        public bool RememberMe { get; set; }

        //public async Task OnGetAsync(string returnUrl = null)
        //{
        //    if (!string.IsNullOrEmpty(ErrorMessage))
        //    {
        //        ModelState.AddModelError(string.Empty, ErrorMessage);
        //    }

        //    returnUrl ??= Url.Content("~/");

        //    // Clear the existing external cookie to ensure a clean login process
        //    await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

        //    ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

        //    ReturnUrl = returnUrl;
        //}
    }
}
