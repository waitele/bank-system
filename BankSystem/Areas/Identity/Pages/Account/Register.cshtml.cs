﻿using System.ComponentModel.DataAnnotations;


namespace BankSystem.Areas.Identity.Pages.Account
{
    public class RegisterModel
    {
        [Required]
        [StringLength(15, ErrorMessage = "{0} turi būti bent {1} ilgio.")]
        [Display(Name = "Vardas")]
        public string Username { get; set; }

        [Required]
        [StringLength(15, ErrorMessage = "The {0} must be at max {1} characters long.")]
        [Display(Name = "Pavardė")]
        public string Usersurname { get; set; }

        [Required]
        [StringLength(19, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 10)]
        [Display(Name = "Asmens Kodas")]
        public string IndentificationNumber { get; set; }

        [Required]
        [Display(Name = "Adresas")]
        public string Address { get; set; }

        [Required]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 7)]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Telefono nr.")]
        public string PhoneNumber { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "El. paštas")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Gimimo d.")]
        public string Birthday { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Slaptažodis")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Patvirtinti slaptažodį")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
