﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(BankSystem.Areas.Identity.IdentityHostingStartup))]
namespace BankSystem.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}