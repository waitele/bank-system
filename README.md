# For database
1. Create folder C:\BankSystemDB
2. Go to project directory in CMD
3. Go to BankSystem folder inside the directory
4. Enter these 3 commands:
	* dotnet tool install --global dotnet-ef
	* dotnet add package Microsoft.EntityFrameworkCore.Design
	* dotnet ef database update

# How to restart database
1. Go to C:\BankSystemDB
2. Delete the database file
3. Open CMD in BankSystem folder of project directory
4. Run command 'dotnet ef database update'